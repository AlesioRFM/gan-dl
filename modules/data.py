import tensorflow as tf

class Embedding:
    def __init__(self, source):

        if source == 'rxrx':
            import modules.theirDataset as dataset
            self._npDataset = dataset.dataset
            self.experiment = dataset.experiment

        if source == 'ours':
            import modules.dataset as dataset
            self._npDataset = dataset.dataset
            self.experiment = dataset.experiment

    @property
    def npDataset(self):
        return iter(self._npDataset)

    def preload(self):
        i=0
        bars = 40
        tot = 38187
        data = self.npDataset
        while True:
            try:
                latents,uids = next(data)
                i+=1
                if(i%20==0):
                    print('\r Step %d/%d '%(i,tot), end='')
                    for j in range(0,round(i/(tot/bars))):
                        print('#',end='')
                    for j in range(round(i/(tot/bars)),bars):
                        print('_',end='')
            except StopIteration:
                break

    def filter(self, filterFunction,labelFunction,nSamples,testing=None,extraFunction=False):
        selectedUids=[]
        data = self.npDataset
        counters=[0,0]
        i=0
        while True:
            if counters[0]>=nSamples/2 and counters[1]>=nSamples/2:
                break

            try:
                latents,uids = next(data)
            except StopIteration:
                break
            
            for j in range(0,len(latents)):
                    latent = latents[j:j+1,:]
                    uid=uids[j]
                    metadata = self.experiment[uid.numpy().decode()]
                    cellLabel = metadata['cell_type']

                    wellHash = hash(metadata['well_id'])
                    
                    if filterFunction(metadata):
                        i=i+1
                        if (testing==False and wellHash%4==0) or (testing==True and wellHash%4!=0):
                            continue

                        label=1
                        labelFuncEval = labelFunction(metadata)
                        if labelFuncEval:
                            label=0

                        if counters[label]<nSamples/2:
                            if extraFunction==False:
                                selectedUids.append({"latent":latent,"label":label,"cell":cellLabel})
                            else:
                                selectedUids.append({"latent":latent,"label":extraFunction(metadata),"cell":cellLabel})

                            counters[label]+=1
                            if counters[label]%10==0:
                                print('\rLoaded %d samples for class 0, %d for class 1'%(counters[0],counters[1]),end="")

        selectedUids.sort(key=lambda a: a["label"])
        return selectedUids