import csv
experiment = {}
with open('metadata.csv', newline='') as csvfile:
	reader = csv.DictReader(csvfile)
	for row in reader:
		experiment[row['site_id']] = row

import tensorflow as tf
physical_devices = tf.config.list_physical_devices('GPU') 
tf.config.experimental.set_memory_growth(physical_devices[0], True)

dataset = tf.data.experimental.make_csv_dataset(
    'ourEmbedding.csv',
    batch_size=1, 
    num_epochs=1,
    label_name='site_id')

def reshapeFeature(features,site_id):
    features_reshaped=tf.concat([feature for feature in features.values()],axis=0)
    site_id = site_id[0]
    return features_reshaped, site_id

dataset = dataset.map(reshapeFeature).shuffle(256).batch(8,drop_remainder=True).prefetch(tf.data.experimental.AUTOTUNE)