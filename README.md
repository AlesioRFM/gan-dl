# GAN-DL
Official repository for https://arxiv.org/abs/2107.07761

Run the main.ipynb jupyter notebook to reproduce the results in the paper
To obtain the results for Recursion AI's method, please download the embeddings from https://www.rxrx.ai/rxrx19a